# Hard way to live w/o Javascript

1. Turn it off in the browser.
   I tried using [GNU Icecat](http://www.gnu.org/software/gnuzilla/)
   but it started failing mysteriously after some time.
   I stopped due to IRL issues.
   Maybe that will change in the future.

2. Do activities besides browsing the web.
   This is both hard and [easy](easy.md) (point 1) because,
   let's face it, the web is both interesting and addictive.
   However, like buddhism, regardless of its truthfulness, it is not
   everything.
