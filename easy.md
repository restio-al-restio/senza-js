# Easy way to live w/o Javascript

1. Don't use the web.
   In my case, i mostly stick to USENET, mailing lists, command-line programs
   (in a Urxvt terminal) and Emacs (w/ and w/o X) for programming, writing,
   and systems administration.
   It (almost) goes w/o saying: my programming does not involve Javascript.

2. Don't use browsers that support Javascript.
   In my case, i use [emacs-w3m](http://emacs-w3m.namazu.org/)
   for most read-only text-heavy sites (e.g., news),
   and [VLC](http://www.videolan.org/)
   via [EMMS](http://www.gnu.org/software/emms)
   for music and video access.
   I used to (in conjunction w/ VLC)
   use [youtube-dl](https://yt-dl.org),
   but that seems to have stopped working of late.
   Update 2020-12-13: youtube-dl seems fine these days.
