# senza-js


__Can one survive the Modern Web™ without Javascript?__

The answer is **YES**!
This project collects notes on the matter.

It's also a practice space for writing Gitlab-compatible markdown.
(Of course that's a dead *irony*, for Gitlab relies on Javascript.)
